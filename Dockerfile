FROM golang:1.22.1-alpine3.19 as build

ENV USER=gitlabmrlabeler
ENV UID=1001
RUN adduser \    
  --disabled-password \    
  --gecos "" \
  --home "/nonexistent" \  
  --shell "/sbin/nologin" \    
  --no-create-home \    
  --uid "${UID}" \
  "${USER}"

WORKDIR /app
COPY . .
RUN go mod download
RUN go mod verify

RUN GOOS=linux go build -ldflags="-w -s" -o gitlab-mr-labeler

FROM alpine:3.19 

COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group

COPY --from=build /app/gitlab-mr-labeler /bin/gitlab-mr-labeler

USER 1001:1001

ENTRYPOINT [ "sh" ]
