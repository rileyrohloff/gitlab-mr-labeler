package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	glClient "gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/gitlab"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/labeler"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/operations"
)

func main() {
	// cli args
	debug := flag.Bool("debug", false, "enables debug logging")
	targetFile := flag.String("f", "labeler.yml", "target file/path arg, default is 'labeler.yml'")
	timeout := flag.Int("timeout", 15, "http request timeout in seconds, default is 15")
	token := flag.String(
		"t",
		"",
		"auth token override arg, defaults to 'LABELER_AUTH_TOKEN' env variable value",
	)
	mergeRequestID := flag.String(
		"mr-id",
		"",
		"target merge-request ID, defaults to CI_MERGE_REQUEST_ID",
	)
	projectID := flag.String(
		"project-id",
		"",
		"target gitlab project ID, defaults to CI_PROJECT_ID",
	)
	authMode := flag.String(
		"auth-mode",
		glClient.PatTokenMode,
		fmt.Sprintf(
			"--auth-mode, -auth-mode tells the labeler to authenticate with a job header or a PAT, default mode is 'pat', possible values are %v | %v",
			glClient.PatTokenMode,
			glClient.JobTokenMode,
		),
	)
	groupID := flag.String(
		"group-id",
		"",
		"target group ID to use when labeler is in 'group' mode, defaults to empty string",
	)

	flag.Parse()

	// log setup
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	log.Debug().Msg("starting gitlab-mr-labeler")

	// config args setup
	authToken := os.Getenv("LABELER_AUTH_TOKEN")
	if len(*token) > 0 {
		authToken = *token
	}

	repoID := os.Getenv("CI_PROJECT_ID")
	if len(*projectID) > 0 {
		repoID = *projectID
	}

	mergeReqID := os.Getenv("CI_MERGE_REQUEST_IID")
	if len(*mergeRequestID) > 0 {
		mergeReqID = *mergeRequestID
	}

	log.Debug().Str("projectID", repoID).Str("merge-request-ID", mergeReqID).Msg("config options")

	// main context setup to be shared
	mainCtx, cncl := context.WithCancel(context.Background())

	// clients setup
	requestClient := http.Client{
		Timeout: time.Duration(*timeout) * time.Second,
	}
	apiClient, err := glClient.NewGitlabClient(
		authToken,
		repoID,
		*groupID,
		mergeReqID,
		*authMode,
		&requestClient,
	)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to create gitlab api client")
	}

	// operations setup
	ops, err := operations.NewLabelerOperations(apiClient)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to setup labelerOperations")
	}

	// labeler setup
	labeler, err := labeler.NewGitlabMergeRequestLabeler(mainCtx, ops, *targetFile)
	if err != nil {
		log.Panic().Err(err).Msg("error building new gitlab client")
	}

	if err := labeler.Run(); err != nil {
		cncl()
		log.Fatal().Err(err).Msg("failed in main Run task")
	}

	log.Info().Msg("finished")
}
