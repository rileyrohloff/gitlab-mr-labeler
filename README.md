
# gitlab-mr-labeler

## Description
Inspired by the github-action [labeler](https://github.com/actions/labeler) project. The intended use for this binary tool is to be used in a CI/CD gitlab pipeline job in either the `.pre` step of gitlab-ci or in any stage of a merge request's pipeline.


#### Legal Disclaimer
This project is in no way associated, backed, or supported by Gitlab the organization.
Support for this project is not covered by any Gitlab license agreement as this is not part of the GitLab Product.

This is a free and open source tool. If you have any issues, please feel free to open one up in this project and I'll be happy to take a look.

## Current State
This project is in active development, however the `main` branch should be in a working state at all times! Install at your discretion. 


Pipeline Status

[![pipeline status](https://gitlab.com/rileyrohloff/gitlab-mr-labeler/badges/main/pipeline.svg)](https://gitlab.com/rileyrohloff/gitlab-mr-labeler/-/commits/main)


Code Coverage

[![coverage report](https://gitlab.com/rileyrohloff/gitlab-mr-labeler/badges/main/coverage.svg)](https://gitlab.com/rileyrohloff/gitlab-mr-labeler/-/commits/main)



## Examples

CLI
`$ gitlab-mr-labeler -f ./workflows/labler.yml --auth=${LABELER_PAT_TOKEN} --merge-request-id=${CI_MERGE_REQUEST_ID}`

#### Example .gitlab-ci.yml Config
```yaml
stages:
  - test

label-merge-request:
  stage: test
  image: golang:1.22-alpine
  script:
    - install the binary or go install it
    - ./gitlab-mr-labeler -f ./labeler-config.yml --debug true
  only:
    refs:
      - merge_requests
```

### Flags description 
Can be ran with `gitlab-mr-labeler -h`
```
-auth-mode string
        --auth-mode, -auth-mode tells the labeler to authenticate with a job header or a PAT, default mode is 'pat', possible valu
es are pat | job-token (default "pat")

-debug
	enables debug logging
	
-f string
	target file/path arg, default is 'labeler.yml' (default "labeler.yml")
	
-group-id string
	target group ID to use when labeler is in 'group' mode, defaults to empty 
	string
	
-mr-id string
	target merge-request ID, defaults to CI_MERGE_REQUEST_ID
	
-project-id string
	target gitlab project ID, defaults to CI_PROJECT_ID
	
-t string
	auth token override arg, defaults to 'CI_JOB_TOKEN' env variable
	
-timeout int
	http request timeout in seconds, default is 15 (default 15)
```
## Install (Goals)

Go Native install

`$ go install gitlab.com/rileyrohloff/gitlab-mr-labeler@main`

Binary install (not actual example just placeholder)

`$ curl -i https://gitlab.com/rileyrohloff/gitlab-mr-labeler/releases/main/binary-${arch} -xo /usr/bin`


## Labeler Configuration File

You must provide the tool with configuration file in the yaml format.

Go Type
```golang
type LabelConfig struct {
	Label Label
	Paths []string
}

type Label struct {
	Name string `json:"name"`
}
```

Example yaml file
```yaml
// /configs/labeler.yml
code:
  - "main.go"
  - "internal/**"

docs:
  - "README.md"
```

Then use the file as a target for the tool to consume with the `-f` argument value

`$ gitlab-mr-labeler -f ./configs/labeler.yml`

### Context
At the time of writing this, my employer is in the process of migrating from Github to Gitlab for various reasons.

When researching to build out the merge-request labeler functionality that we have in github, we determined there wasn't a valid project yet running for this feature being available in gitlab.
Since gitlab doesn't yet support [this feature](https://gitlab.com/gitlab-org/gitlab/-/issues/30748) and after some initial research I haven't been able to find anything public that does MR labeling out of the box. 

My goal with this project is to provide a easy to use CLI that can re-use the
same `labeler.yml` file that the github-action open source project uses to achieve automated MR labeling in a gitlab-ci pipeline. 

### Authentication

###### Sub-context on why we need to use a PAT (Optional Read)
This tool was built with the idea in mind that it will most likely be ran within the context of a Gitlab CI/CD job. Therefore authenticating to the Gitlab API via the `CI_JOB_TOKEN` env variable. However, at the time of writing this, the CI_JOB_TOKEN environment variable has [limited scope](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html) compared to a PAT. 

In this case, specifically the `labels` API endpoints are both private and not included in the list of available endpoints for the `CI_JOB_TOKEN` provided.

I only learned this after developing locally with a PAT and then finally testing in a real pipeline with the job token. 

I still have built in support for using job tokens at if at some point in the future the `labels` API either becomes publicly accessible or added to the list of available endpoints for the job token.

##### Auth Tokens
You must provide the labeler with some form of a project scoped access token like a [PAT](https://docs.gitlab.com/ee/security/token_overview.html#personal-access-tokens) in order to authenticate to obtain a project or group's labels. The labels API is not publicly available and the gitlab [job-token](https://docs.gitlab.com/ee/security/token_overview.html#cicd-job-tokens) cannot authenticate to the [labels](https://docs.gitlab.com/ee/api/labels.html) API endpoints.

You can provide the auth token in two different methods

##### Method 1: Environment Variable
`$ LABELER_AUTH_TOKEN=my_pat_from_gitlab gitlab-mr-labeler -f labelConfig.yaml`

##### Method 2: Pass via CLI argument
`$ gitlab-mr-labeler -f labelConfig.yml -t my_pat_from_gitlab --debug true`


## Current known alternatives
- There is a project called [Triage and Label](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) under the gitlab-org group of Gitlab.com, however the [project isn't officially supported by gitlab](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#note-this-gem-is-not-supported-by-the-gitlab-support-team)
	- The project is a ruby gem and appears to do quite a bit more than just labeling Merge Requests which doesn't fit the smaller use case intended for this project.
- Rolling your own webhook server to receive incoming requests from gitlab pipeline updates. [Blog post from gitlab](https://about.gitlab.com/blog/2016/08/19/applying-gitlab-labels-automatically/)

## Container Image (Not yet implemented)

Docker

`$ docker run -it --entrypoint sh registry.gitlab.com/rileyrohloff/gitlab-mr-labeler:latest`


.gitlab-ci.yml
```yml
image: registry.gitlab.com/rileyrohloff/gitlab-mr-labeler:latest
stage: .pre
when:
  - merge_request
script:
  - gitlab-mr-labeler -f path/labeler.yml -token ${LABELER_PAT_TOKEN} --merge-request-id ${CI_MERGE_REQUEST_ID}
```

