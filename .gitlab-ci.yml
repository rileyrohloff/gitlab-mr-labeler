# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
#

include:
- template: Security/SAST.gitlab-ci.yml

stages:
- container-build
- scan
- test

# sast:
#   stage: test
#   rules:
#     - if: $CI_COMMIT_BRANCH == "main"

.kaniko-container-build:
  stage: container-build
  variables:
    BRANCH_TAG: ${CI_COMMIT_REF_SLUG}
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}:${BRANCH_TAG}"
      --tarPath=${CI_PROJECT_NAME}-${CI_COMMIT_SHA}.tar
  artifacts:
    paths:
      - "*.tar"

 

docker-branch-build:
  extends:
    - .kaniko-container-build
  only:
    refs:
      - merge_requests


docker-branch-latest:
  variables:
    BRANCH_TAG: latest
  extends:
    - .kaniko-container-build
  only:
    refs:
      - main

container-scan:
  stage: scan 
  artifacts:
    paths:
      - gl-codeclimate.json
    reports:
      codequality: gl-codeclimate.json
  image: 
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - trivy image --clear-cache
    - trivy image --download-db-only
    - trivy image --scanners vuln --input ${CI_PROJECT_NAME}-${CI_COMMIT_SHA}.tar --format template --template "@/contrib/gitlab-codequality.tpl" -o gl-codeclimate-image.json
    - trivy filesystem --scanners vuln --exit-code 0 --format template --template "@/contrib/gitlab-codequality.tpl" -o gl-codeclimate-fs.json ${CI_PROJECT_DIR}
    - apk update && apk add jq
    - jq -s 'add' gl-codeclimate-image.json gl-codeclimate-fs.json > gl-codeclimate.json
    - trivy image --input ${CI_PROJECT_NAME}-${CI_COMMIT_SHA}.tar --exit-code 1 --severity CRITICAL
    - trivy filesystem --scanners vuln --exit-code 1 --severity CRITICAL ${CI_PROJECT_DIR}
  only:
    refs:
      - main
      - merge_requests

go-test:
  stage: test
  image: golang:1.22-alpine
  only:
    refs:
      - main
      - merge_requests
  script:
    - go install
    - go test -v ./... -coverprofile=coverage.txt -covermode count
    - go get github.com/boumenot/gocover-cobertura
    - go run github.com/boumenot/gocover-cobertura < coverage.txt > coverage.xml
  coverage: '/^coverage: (\d+.\d+)% of statements$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

go-lint:
  stage: test
  image: golangci/golangci-lint:v1.56.2-alpine
  only:
    refs:
      - main
      - merge_requests
  script:
    - golangci-lint run -v

go-test-build:
  stage: test
  image: golang:1.22-alpine
  only:
    refs:
      - main
  script:
    - go build -o gitlab-mr-labeler 
    - ls -la

label-merge-request:
  stage: .pre 
  image: golang:1.22-alpine
  script:
    - apk add curl
    - echo "Validating provided token is valid"
    - curl -i --header "PRIVATE-TOKEN:${LABELER_AUTH_TOKEN}" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/labels
    - go get ./...
    - go build -o gitlab-mr-labeler
    - ./gitlab-mr-labeler -f ./labeler-config.yml -t ${LABELER_AUTH_TOKEN} --debug true
  only:
    refs:
      - merge_requests
