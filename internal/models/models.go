package models

import "net/http"

type Label struct {
	Name string `json:"name"`
}

type LabelConfig struct {
	Label Label
	Paths []string
}

type MergeRequestDiffs []MergeRequestDiff

type MergeRequestDiff struct {
	NewPath string `json:"new_path"`
	OldPath string `json:"old_path"`
}

//go:generate mockery --name LabelerHTTPClient
type LabelerHTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}
