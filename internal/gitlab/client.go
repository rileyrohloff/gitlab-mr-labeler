package gitlab

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"slices"
	"strconv"

	"github.com/rs/zerolog/log"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models"
)

var (
	BaseURL              = "https://gitlab.com/api/v4"
	MergeRequestDiffsURI = "/projects/%v/merge_requests/%v/diffs"
	MergeRequestURI      = "/projects/%v/merge_requests/%v"
	GroupLabelsURI       = "/groups/%v/labels"
	ProjectLabelsURI     = "/projects/%v/labels"
	ProjectMode          = "project"
	GroupMode            = "group"
	PatTokenMode         = "pat"
	JobTokenMode         = "job-token"
	PerPageCount         = 30
)

type APIClient struct {
	httpClient     models.LabelerHTTPClient
	authToken      string
	projectID      string
	mergeRequestID string
	groupID        string
	labelerMode    string
	authMode       string
}

func NewGitlabClient(
	token, projID, groupID, mrID, authMode string,
	httpClient models.LabelerHTTPClient,
) (APIClient, error) {
	var glClient APIClient

	if token == "" {
		return glClient, errors.New("invalid token")
	}

	if projID == "" {
		return glClient, errors.New("invalid projectID")
	}

	if mrID == "" {
		return glClient, errors.New("invalid mergeRequestID")
	}

	if len(groupID) > 0 && len(projID) > 0 {
		return glClient, errors.New(
			"both groupID and projectID provided, can only pull labels from a single source",
		)
	}

	if projID != "" {
		glClient.labelerMode = ProjectMode
	}

	if groupID != "" {
		glClient.labelerMode = GroupMode
	}

	if authMode != PatTokenMode && authMode != JobTokenMode {
		return glClient, fmt.Errorf(
			"invalid value for 'authMode', must be %v or %v, received: %v",
			PatTokenMode,
			JobTokenMode,
			authMode,
		)
	}

	glClient.httpClient = httpClient
	glClient.authToken = token
	glClient.projectID = projID
	glClient.mergeRequestID = mrID
	glClient.groupID = groupID
	glClient.authMode = authMode

	return glClient, nil
}

func (client *APIClient) SetUpHeaders(r *http.Request) {
	r.Header.Add("Content-type", "application/json")

	switch client.authMode {
	case PatTokenMode:
		r.Header.Add("PRIVATE-TOKEN", client.authToken)
	case JobTokenMode:
		r.Header.Add("JOB-TOKEN", client.authToken)
	}
}

func (client *APIClient) GetMergeRequestData(
	ctx context.Context,
) ([]models.MergeRequestDiff, error) {
	log.Debug().Msgf("requesting data for merge_request: %v", client.mergeRequestID)

	path := fmt.Sprintf(
		MergeRequestDiffsURI,
		client.projectID,
		client.mergeRequestID,
	)
	url := fmt.Sprint(BaseURL, path)

	log.Debug().Str("url", url).Msg("constructed url")

	responseData := make([]models.MergeRequestDiff, 0)

	page := 1

	for {
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
		if err != nil {
			return nil, fmt.Errorf("error settup up http request: %w", err)
		}

		client.SetUpHeaders(req)
		params := req.URL.Query()
		pageStr := strconv.Itoa(page)
		perPageCountStr := strconv.Itoa(PerPageCount)

		params.Add("page", pageStr)
		params.Add("per_page", perPageCountStr)

		req.URL.RawQuery = params.Encode()

		log.Debug().Str("url", req.URL.String()).Msg("getting diffs")

		diffs, err := client.GetMergeRequestDiff(req)
		if err != nil {
			return nil, fmt.Errorf("error in request getting diffs: %w", err)
		}

		responseData = slices.Concat(responseData, diffs)

		if len(diffs) < PerPageCount {
			break
		}

		page++
	}

	return responseData, nil
}

func (client *APIClient) GetMergeRequestDiff(
	req *http.Request,
) (data []models.MergeRequestDiff, err error) {
	resp, err := client.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error in GET request: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"received non-ok status code when getting merge-request API: %v",
			resp.Status,
		)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error readying body: %w", err)
	}

	diffs := models.MergeRequestDiffs{}

	if err := json.Unmarshal(body, &diffs); err != nil {
		return nil, fmt.Errorf("error unmarshalling json: %w", err)
	}

	for _, val := range diffs {
		data = append(data, val)
	}

	return data, nil
}

func (client *APIClient) UpdateMergeRequestLabels(
	ctx context.Context,
	labels map[string]string,
) error {
	log.Debug().Msgf("attempting to update with labels merge_request: %v", client.mergeRequestID)

	path := fmt.Sprintf(
		MergeRequestURI,
		client.projectID,
		client.mergeRequestID,
	)
	url := fmt.Sprint(BaseURL, path)

	marshalled, err := json.Marshal(labels)
	if err != nil {
		return fmt.Errorf("error marshalling labels data: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewReader(marshalled))
	if err != nil {
		return fmt.Errorf("error constructuing request to update MR: %w", err)
	}

	client.SetUpHeaders(req)

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf(
			"error in update request updating labels for merge_request: %v, err: %w",
			client.mergeRequestID,
			err,
		)
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("received non-ok status from %v, status code: %v", url, resp.Status)
	}

	log.Debug().
		Str("merge_request_id", client.mergeRequestID).
		Msg("successfully updated merge-request labels")

	return nil
}

func (client *APIClient) GetLabels(ctx context.Context) ([]models.Label, error) {
	log.Debug().Msgf("fetching labels from the %v level", client.labelerMode)

	var path string

	// default labeler mode is project so we're matching that here
	path = fmt.Sprintf(
		ProjectLabelsURI,
		client.projectID,
	)

	// go to group level if specified my labeler mode
	if client.labelerMode == GroupMode {
		path = fmt.Sprintf(
			GroupMode,
			client.projectID,
		)
	}

	url := fmt.Sprint(BaseURL, path)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("error constructing http request: %w", err)
	}

	client.SetUpHeaders(req)

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error making http request: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"received non-ok status code: url: %v, statusCode: %v, err: %w",
			url,
			resp.Status,
			err,
		)
	}

	respData := make([]models.Label, 0)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response data: %w", err)
	}

	if err := json.Unmarshal(body, &respData); err != nil {
		return nil, fmt.Errorf("error marshalling JSON to response destination: %w", err)
	}

	log.Debug().Msg("fetched label data")

	return respData, nil
}
