package gitlab_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/gitlab"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models/mocks"
)

var (
	BaseURL              = "https://gitlab.com/api/v4"
	MergeRequestDiffsURI = "/projects/%v/merge_requests/%v/diffs"
	MergeRequestURI      = "/projects/%v/merge_requests/%v"
	groupID              = ""
)

func TestNewAPIClient(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	mockClient := mocks.NewLabelerHTTPClient(t)

	client, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.Nil(err)
	assert.NoError(err)
	assert.NotNil(client)

	client2, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.PatTokenMode,
		mockClient,
	)
	assert.Nil(err)
	assert.NoError(err)
	assert.NotNil(client2)

	_, err = gitlab.NewGitlabClient("", projID, groupID, mrID, gitlab.JobTokenMode, mockClient)
	assert.Error(err)
	assert.ErrorContains(err, "invalid token")

	_, err = gitlab.NewGitlabClient(token, "", mrID, groupID, gitlab.JobTokenMode, mockClient)
	assert.Error(err)
	assert.ErrorContains(err, "invalid projectID")

	_, err = gitlab.NewGitlabClient(token, projID, groupID, "", gitlab.JobTokenMode, mockClient)
	assert.Error(err)
	assert.ErrorContains(err, "invalid mergeRequestID")

	_, err = gitlab.NewGitlabClient(token, projID, "adds", mrID, gitlab.JobTokenMode, mockClient)
	assert.Error(err)
	assert.ErrorContains(err, "both groupID and projectID provided,")

	_, err = gitlab.NewGitlabClient(token, projID, "", mrID, "CINotReal", mockClient)
	assert.Error(err)
	assert.ErrorContains(err, "CINotReal")
}

func TestSetupRequestHeaders_JobTokenMode(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	client, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		&http.Client{},
	)
	assert.Nil(err)
	assert.NoError(err)
	assert.NotNil(client)

	req, err := http.NewRequestWithContext(testCtx, http.MethodGet, gitlab.BaseURL, nil)
	assert.Nil(err)

	patTokenHeader := req.Header.Get("PRIVATE-TOKEN")
	assert.Equal(patTokenHeader, "")

	// jobTokenHeader := req.Header.Get("JOB-TOKEN")
	// assert.Len(jobTokenHeader, 0)

	client.SetUpHeaders(req)

	// jobTokenHeader = req.Header.Get("JOB-TOKEN")
	patTokenHeader = req.Header.Get("PRIVATE-TOKEN")

	// assert.Equal(jobTokenHeader, token)
	assert.Equal(patTokenHeader, "")
}

func TestSetupRequestHeaders_PatTokenMode(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	client, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.PatTokenMode,
		&http.Client{},
	)
	assert.Nil(err)
	assert.NoError(err)
	assert.NotNil(client)

	req, err := http.NewRequestWithContext(testCtx, http.MethodGet, gitlab.BaseURL, nil)
	assert.Nil(err)

	patTokenHeader := req.Header.Get("PRIVATE-TOKEN")
	assert.Equal(patTokenHeader, "")

	client.SetUpHeaders(req)

	patTokenHeader = req.Header.Get("PRIVATE-TOKEN")

	assert.Equal(patTokenHeader, token)
}

func TestGetMergeRequestData_Success(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.MergeRequestDiffsURI,
		projID,
		mrID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	mockRespData := []map[string]string{{"old_path": "/test/*", "new_path": "/test/*"}}
	mockJSONString, err := json.Marshal(mockRespData)
	assert.NoError(err)
	mockRespBody := io.NopCloser(bytes.NewReader(mockJSONString))
	mockRequest, err := http.NewRequestWithContext(testCtx, http.MethodGet, url, nil)
	api.SetUpHeaders(mockRequest)
	assert.NoError(err)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       mockRespBody,
	}

	mockClient.On("Do", mock.Anything).Return(mockResp, nil)

	mrData, err := api.GetMergeRequestData(testCtx)
	assert.NotNil(mrData)
	assert.NoError(err)
	assert.Equal(mrData[0].NewPath, mockRespData[0]["new_path"])
}

func TestGetMergeRequestData_Pagination(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.MergeRequestDiffsURI,
		projID,
		mrID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	paginatedData := make([]models.MergeRequestDiff, 0)

	mockRespData := models.MergeRequestDiff{
		NewPath: "/test/test",
		OldPath: "/test/test",
	}

	for i := 0; i < 35; i++ {
		paginatedData = append(paginatedData, mockRespData)
	}

	assert.Len(paginatedData, 35)

	page1MockJSONString, err := json.Marshal(paginatedData[0:30])
	assert.NoError(err)
	page1Body := io.NopCloser(bytes.NewReader(page1MockJSONString))
	firstMockRequest, err := http.NewRequestWithContext(testCtx, http.MethodGet, url, nil)
	assert.NoError(err)

	page2MockJSONString, err := json.Marshal(paginatedData[30:35])
	assert.NoError(err)
	page2Body := io.NopCloser(bytes.NewReader(page2MockJSONString))
	secondMockRequest, err := http.NewRequestWithContext(testCtx, http.MethodGet, url, nil)
	assert.NoError(err)

	api.SetUpHeaders(secondMockRequest)
	api.SetUpHeaders(firstMockRequest)

	params := secondMockRequest.URL.Query()

	params.Add("page", "1")
	params.Add("per_page", "30")
	firstMockRequest.URL.RawQuery = params.Encode()

	secondParams := secondMockRequest.URL.Query()

	secondParams.Add("page", "2")
	secondParams.Add("per_page", "30")
	secondMockRequest.URL.RawQuery = secondParams.Encode()

	page1MockResp := &http.Response{
		StatusCode: 200,
		Body:       page1Body,
	}

	page2MockResp := &http.Response{
		StatusCode: 200,
		Body:       page2Body,
	}

	mockClient.EXPECT().Do(firstMockRequest).Return(page1MockResp, nil)
	mockClient.EXPECT().Do(secondMockRequest).Return(page2MockResp, nil)

	mrData, err := api.GetMergeRequestData(testCtx)
	assert.Nil(err)
	assert.Equal(mrData, paginatedData)
}

func TestGetMergeRequestData_Pagination_PageCountCase(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	paginatedData := make([]models.MergeRequestDiff, 0)

	mockRespData := models.MergeRequestDiff{
		NewPath: "/test/test",
		OldPath: "/test/test",
	}

	for i := 0; i < 30; i++ {
		paginatedData = append(paginatedData, mockRespData)
	}

	assert.Len(paginatedData, 30)

	page1MockJSONString, err := json.Marshal(paginatedData)
	assert.NoError(err)
	page1Body := io.NopCloser(bytes.NewReader(page1MockJSONString))
	page2Body := io.NopCloser(bytes.NewReader([]byte("[]")))

	page1MockResp := &http.Response{
		StatusCode: 200,
		Body:       page1Body,
	}

	page2MockResp := &http.Response{
		StatusCode: 200,
		Body:       page2Body,
	}

	mockClient.On("Do", mock.Anything).Return(page1MockResp, nil).Once()
	mockClient.On("Do", mock.Anything).Return(page2MockResp, nil)

	mrData, err := api.GetMergeRequestData(testCtx)
	assert.Nil(err)
	assert.Equal(paginatedData, mrData)
}

func TestGetMergeRequestData_Failure(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.MergeRequestDiffsURI,
		projID,
		mrID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	mockRespData := []map[string]string{{"old_path": "/test/*", "new_path": "/test/*"}}
	mockJSONString, err := json.Marshal(mockRespData)
	assert.NoError(err)
	mockRespBody := io.NopCloser(bytes.NewReader(mockJSONString))
	mockRequest, err := http.NewRequestWithContext(testCtx, http.MethodGet, url, nil)
	api.SetUpHeaders(mockRequest)
	assert.NoError(err)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       mockRespBody,
	}

	mockError := errors.New("client error")

	mockClient.On("Do", mock.Anything).Return(mockResp, mockError)

	mrData, err := api.GetMergeRequestData(testCtx)
	assert.Nil(mrData)
	assert.Error(err)
	assert.ErrorContains(err, "error in GET request")
}

func TestUpdateMergeRequestLabels_Success(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.MergeRequestURI,
		projID,
		mrID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	testLabels := "devops,infrastructure"
	mockReqData := map[string]string{"labels": testLabels}
	mockJSONString, err := json.Marshal(mockReqData)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		bytes.NewReader(mockJSONString),
	)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(mockJSONString)),
	}

	mockClient.On("Do", mock.Anything, mock.Anything).Return(mockResp, nil)

	err = api.UpdateMergeRequestLabels(testCtx, mockReqData)
	assert.NoError(err)
}

func TestUpdateMergeRequestLabels_ClientError(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.MergeRequestURI,
		projID,
		mrID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	testLabels := "devops,infrastructure"
	mockReqData := map[string]string{"labels": testLabels}
	mockJSONString, err := json.Marshal(mockReqData)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		bytes.NewReader(mockJSONString),
	)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(mockJSONString)),
	}

	mockError := errors.New("client error")

	mockClient.On("Do", mock.Anything, mock.Anything).Return(mockResp, mockError)

	err = api.UpdateMergeRequestLabels(testCtx, mockReqData)
	assert.Error(err)
	assert.ErrorContains(err, "error in update request")
}

func TestUpdateMergeRequestLabels_BadStatusCode(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.MergeRequestURI,
		projID,
		mrID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	testLabels := "devops,infrastructure"
	mockReqData := map[string]string{"labels": testLabels}
	mockJSONString, err := json.Marshal(mockReqData)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		bytes.NewReader(mockJSONString),
	)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 401,
		Body:       io.NopCloser(bytes.NewReader(mockJSONString)),
	}

	mockClient.On("Do", mock.Anything, mock.Anything).Return(mockResp, nil)

	err = api.UpdateMergeRequestLabels(testCtx, mockReqData)
	assert.Error(err)
	assert.ErrorContains(err, "non-ok status")
}

func TestGetGroupLabels_Success(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.GroupLabelsURI,
		projID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		nil,
	)
	assert.NoError(err)

	labelData := []models.Label{
		{
			Name: "devops",
		},
		{
			Name: "infrastructure",
		},
	}

	byteStr, err := json.Marshal(labelData)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(byteStr)),
	}

	mockClient.On("Do", mock.Anything).Return(mockResp, nil)

	resp, err := api.GetLabels(testCtx)
	assert.NoError(err)
	assert.Nil(err)
	assert.NotNil(resp)
	assert.Equal(resp, labelData)
}

func TestGetGroupLabels_BadResponse(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.GroupLabelsURI,
		projID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		nil,
	)
	assert.NoError(err)

	labelData := map[string]string{
		"id":   "4321",
		"name": "infrastructure",
	}

	byteStr, err := json.Marshal(labelData)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(byteStr)),
	}

	mockClient.On("Do", mock.Anything).Return(mockResp, nil)

	_, err = api.GetLabels(testCtx)
	assert.Error(err)
	assert.ErrorContains(err, "error marshalling JSON")
}

func TestGetGroupLabels_Failure(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.GroupLabelsURI,
		projID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		nil,
	)
	assert.NoError(err)

	labelData := []map[string]string{
		{
			"id":   "1234",
			"name": "devops",
		},
		{
			"id":   "4321",
			"name": "infrastructure",
		},
	}

	byteStr, err := json.Marshal(labelData)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 401,
		Body:       io.NopCloser(bytes.NewReader(byteStr)),
	}

	mockClient.On("Do", mock.Anything).Return(mockResp, nil)

	_, err = api.GetLabels(testCtx)
	assert.Error(err)
	assert.ErrorContains(err, "received non-ok")
}

func TestGetGroupLabels_ClientFailure(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	mockClient := mocks.NewLabelerHTTPClient(t)

	assert := assert.New(t)

	token := "test"
	projID := "test"
	mrID := "test"

	path := fmt.Sprintf(
		gitlab.GroupLabelsURI,
		projID,
	)
	url := fmt.Sprint(BaseURL, path)

	api, err := gitlab.NewGitlabClient(
		token,
		projID,
		groupID,
		mrID,
		gitlab.JobTokenMode,
		mockClient,
	)
	assert.NoError(err)

	mockRequest, err := http.NewRequestWithContext(
		testCtx,
		http.MethodPut,
		url,
		nil,
	)
	assert.NoError(err)

	labelData := []map[string]string{
		{
			"id":   "1234",
			"name": "devops",
		},
		{
			"id":   "4321",
			"name": "infrastructure",
		},
	}

	byteStr, err := json.Marshal(labelData)
	assert.NoError(err)

	api.SetUpHeaders(mockRequest)

	mockResp := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(byteStr)),
	}

	mockErr := fmt.Errorf("client error")

	mockClient.On("Do", mock.Anything).Return(mockResp, mockErr)

	_, err = api.GetLabels(testCtx)
	assert.Error(err)
	assert.ErrorIs(err, mockErr)
}
