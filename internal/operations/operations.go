package operations

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/gitlab"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models"
)

// var (
// 	ReplacePattern = ``
// )

type LabelerOperations struct {
	apiClient gitlab.APIClient
}

func NewLabelerOperations(
	client gitlab.APIClient,
) (ops LabelerOperations, err error) {
	ops.apiClient = client

	return ops, nil
}

func (lOps *LabelerOperations) GetUpstreamLabels(
	ctx context.Context,
) (labels []models.Label, err error) {
	log.Info().Msg("fetching labels")

	labelData, err := lOps.apiClient.GetLabels(ctx)
	if err != nil {
		return labels, fmt.Errorf("client error: %w", err)
	}

	return labelData, nil
}

func (lOps *LabelerOperations) IsConfigValid(
	fetchedConfig []models.Label, providedConfig []models.LabelConfig,
) bool {
	valid := true

	for _, configVal := range providedConfig {
		found := false
		providedName := configVal.Label.Name

		for _, fetchedVal := range fetchedConfig {
			if providedName == fetchedVal.Name {
				found = true

				continue
			}
		}

		if !found {
			valid = false

			break
		}
	}

	return valid
}

func (lOps *LabelerOperations) SatisfiesLabelConfig(
	labelConfig models.LabelConfig,
	diffs []models.MergeRequestDiff,
) (bool, error) {
	satisfied := false

	for _, path := range labelConfig.Paths {
		// come back to this after I get better at regex
		expression := strings.ReplaceAll(path, "**", ".")
		for _, diff := range diffs {
			newPathMatch, err := regexp.MatchString(expression, diff.NewPath)
			if err != nil {
				return false, fmt.Errorf("error matching regular expression: %w", err)
			}

			oldpathMatch, err := regexp.MatchString(expression, diff.NewPath)
			if err != nil {
				return false, fmt.Errorf("error matching regular expression: %w", err)
			}

			if newPathMatch || oldpathMatch {
				satisfied = true

				break
			}
		}
	}

	return satisfied, nil
}

func (lOps *LabelerOperations) ReconcileDiffs(
	ctx context.Context,
	providedConfig []models.LabelConfig,
) (labels []models.Label, err error) {
	mrDiffs, err := lOps.apiClient.GetMergeRequestData(ctx)
	if err != nil {
		return labels, fmt.Errorf("error in apiclient getting merge request diffs: %w", err)
	}

	for _, config := range providedConfig {
		needsLabel, err := lOps.SatisfiesLabelConfig(config, mrDiffs)
		if err != nil {
			log.Error().Err(err).Msg("error comparing label config to merge request diff")

			continue
		}

		if needsLabel {
			labels = append(labels, config.Label)
		}
	}

	return labels, nil
}

func SquashLabelsToString(labels []models.Label) (labelsString string) {
	labelNames := make([]string, 0)

	for _, val := range labels {
		labelNames = append(labelNames, val.Name)
	}

	labelsString = strings.Join(labelNames, ",")

	return labelsString
}

func (lOps *LabelerOperations) ApplyLabels(ctx context.Context, labels []models.Label) error {
	labelString := SquashLabelsToString(labels)

	applyLabelConfig := map[string]string{"labels": labelString}
	if err := lOps.apiClient.UpdateMergeRequestLabels(ctx, applyLabelConfig); err != nil {
		return fmt.Errorf("error in apiclient updating merge request: %w", err)
	}

	return nil
}

func (lOps *LabelerOperations) RunLabelerOperation(
	ctx context.Context,
	labelConfigs []models.LabelConfig,
) error {
	neededLabels, err := lOps.ReconcileDiffs(ctx, labelConfigs)
	if err != nil {
		return fmt.Errorf("error reconciling diffs: %w", err)
	}

	// return out if no labels are needed to apply
	if len(neededLabels) == 0 {
		log.Debug().Msg("evaluated MR needed zero labels applied")

		return nil
	}

	if err := lOps.ApplyLabels(ctx, neededLabels); err != nil {
		return fmt.Errorf("error in apply labels operation: %w", err)
	}

	return nil
}
