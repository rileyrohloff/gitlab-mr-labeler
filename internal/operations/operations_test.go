package operations_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/gitlab"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models/mocks"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/operations"
)

var groupID = ""

func TestGetUpstreamLabels(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	assert := assert.New(t)

	token := "test"

	mockClient := mocks.NewLabelerHTTPClient(t)

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		mockClient,
	)
	if err != nil {
		t.Fail()
	}

	mockResponseData := []models.Label{
		{Name: "devops"},
		{Name: "test"},
	}

	mockJSONString, err := json.Marshal(mockResponseData)
	assert.Nil(err)

	mockHTTPResponse := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(mockJSONString)),
	}

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	mockClient.On("Do", mock.Anything).Return(mockHTTPResponse, nil)

	data, err := ops.GetUpstreamLabels(testCtx)
	assert.Nil(err)
	assert.Equal(data, mockResponseData)
}

func TestGetUpstreamLabels_Failures(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()

	assert := assert.New(t)

	token := "test"

	mockClient := mocks.NewLabelerHTTPClient(t)

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		mockClient,
	)
	if err != nil {
		t.Fail()
	}

	mockResponseData := []models.Label{
		{Name: "devops"},
		{Name: "test"},
	}

	mockJSONString, err := json.Marshal(mockResponseData)
	assert.Nil(err)

	mockHTTPResponse := &http.Response{
		StatusCode: 401,
		Body:       io.NopCloser(bytes.NewReader(mockJSONString)),
	}

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	mockClient.On("Do", mock.Anything).Return(mockHTTPResponse, nil)

	_, err = ops.GetUpstreamLabels(testCtx)
	assert.Error(err)
	assert.ErrorContains(err, "non-ok")
}

func TestValidateLabels(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)

	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	fetchedLabels := []models.Label{
		{Name: "devops"},
		{Name: "infrastructure"},
		{Name: "security"},
	}

	providedConfig := []models.LabelConfig{
		{Label: models.Label{Name: "devops"}, Paths: []string{"/path/2"}},
		{Label: models.Label{Name: "infrastructure"}, Paths: []string{"/path/2"}},
	}

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	valid1 := ops.IsConfigValid(fetchedLabels, providedConfig)

	assert.True(valid1)

	badConfig := []models.LabelConfig{
		{Label: models.Label{Name: "badval"}, Paths: []string{"/path/2"}},
		{Label: models.Label{Name: "infrastructure"}, Paths: []string{"/path/2"}},
	}
	valid2 := ops.IsConfigValid(fetchedLabels, badConfig)

	assert.False(valid2)

	badConfig2 := []models.LabelConfig{
		{Label: models.Label{Name: "badval"}, Paths: []string{"/path/2"}},
		{Label: models.Label{Name: "infrastructure"}, Paths: []string{"/path/2"}},
	}
	valid3 := ops.IsConfigValid(fetchedLabels, badConfig2)

	assert.False(valid3)
}

func TestSatisfiesLabelConfig(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)

	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	providedConfig := &models.LabelConfig{
		Label: models.Label{Name: "devops"},
		Paths: []string{
			"path/**",
			"/nopath/*",
		},
	}

	mockDiffs := []models.MergeRequestDiff{
		{NewPath: "path/1", OldPath: "path1/1"},
	}

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	needsLabels, err := ops.SatisfiesLabelConfig(*providedConfig, mockDiffs)
	assert.Nil(err)
	assert.True(needsLabels)

	mockDiffs2 := []models.MergeRequestDiff{
		{NewPath: "/nopath/1", OldPath: "/nopath/sub/path/test"},
	}

	needsLabels2, err := ops.SatisfiesLabelConfig(*providedConfig, mockDiffs2)
	assert.Nil(err)
	assert.True(needsLabels2)

	mockDiffs3 := []models.MergeRequestDiff{
		{NewPath: "/not/tracked/1", OldPath: "/nottracked/2"},
		{NewPath: "/not/tracked/1", OldPath: "/nottracked/2"},
		{NewPath: "/moved/tf.yml", OldPath: "/new/path/tf.yml"},
	}

	noChanges, err := ops.SatisfiesLabelConfig(*providedConfig, mockDiffs3)
	assert.Nil(err)
	assert.False(noChanges)

	wildCardConfig := models.LabelConfig{
		Label: models.Label{Name: "devops"},
		Paths: []string{
			"path/**",
			"/nopath/*",
		},
	}

	wildCardDiffs := []models.MergeRequestDiff{
		{NewPath: "/not/tracked/1", OldPath: "/nottracked/2"},
		{NewPath: "/not/tracked/1", OldPath: "/nottracked/2"},
		{NewPath: "/moved/tf.yml", OldPath: "/new/path/tf.yml"},
		{NewPath: "path/sub/path/test.yml", OldPath: "/new/path/tf.yml"},
	}

	wildCardChanges, err := ops.SatisfiesLabelConfig(wildCardConfig, wildCardDiffs)
	assert.Nil(err)
	assert.True(wildCardChanges)
}

func TestReconcileDiffs(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()
	assert := assert.New(t)

	token := "test"

	mockAPIClient := mocks.NewLabelerHTTPClient(t)

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		mockAPIClient,
	)
	if err != nil {
		t.Fail()
	}

	providedConfig := []models.LabelConfig{
		{
			Label: models.Label{Name: "devops"},
			Paths: []string{
				"/path/*/*",
				"/nopath/*",
			},
		},
		{
			Label: models.Label{Name: "security"},
			Paths: []string{
				"/terraform/*",
				"/config/path/*",
			},
		},
	}

	devopsDiffs := []models.MergeRequestDiff{
		{NewPath: "/path/1", OldPath: "/path/1"},
	}

	mockDiffsJSONString, err := json.Marshal(devopsDiffs)
	assert.Nil(err)

	mockHTTPResponse := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(mockDiffsJSONString)),
	}

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	mockAPIClient.On("Do", mock.Anything).Return(mockHTTPResponse, nil)

	neededLabels, err := ops.ReconcileDiffs(testCtx, providedConfig)
	assert.Nil(err)
	assert.Len(neededLabels, 1)
	assert.Equal(neededLabels[0].Name, "devops")
}

func TestReconcileDiffs_2(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()
	assert := assert.New(t)

	token := "test"

	mockAPIClient := mocks.NewLabelerHTTPClient(t)

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		mockAPIClient,
	)
	if err != nil {
		t.Fail()
	}

	providedConfig := []models.LabelConfig{
		{
			Label: models.Label{Name: "devops"},
			Paths: []string{
				"/path/*",
				"/nopath/*",
			},
		},
		{
			Label: models.Label{Name: "security"},
			Paths: []string{
				"/terraform/*",
				"/config/path/*",
			},
		},
	}

	securityDevOpsDiffs := []models.MergeRequestDiff{
		{NewPath: "/terraform/1", OldPath: "/not/tracked/1"},
		{NewPath: "/path/1", OldPath: "/path/1"},
	}

	mockDiffsJSONString, err := json.Marshal(securityDevOpsDiffs)
	assert.Nil(err)

	mockHTTPResponse2 := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(mockDiffsJSONString)),
	}

	mockAPIClient.On("Do", mock.Anything).Return(mockHTTPResponse2, nil)

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	neededLabels2, err := ops.ReconcileDiffs(testCtx, providedConfig)
	assert.Nil(err)
	assert.Len(neededLabels2, 2)
	assert.Equal(neededLabels2[1].Name, "security")
}

func TestReconcileDiffs_ClientFailues(t *testing.T) {
	t.Parallel()

	testCtx := context.Background()
	assert := assert.New(t)

	token := "test"

	mockAPIClient := mocks.NewLabelerHTTPClient(t)

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		mockAPIClient,
	)
	if err != nil {
		t.Fail()
	}

	providedConfig := []models.LabelConfig{
		{
			Label: models.Label{Name: "devops"},
			Paths: []string{
				"/path/*",
				"/nopath/*",
			},
		},
		{
			Label: models.Label{Name: "security"},
			Paths: []string{
				"/terraform/*",
				"/config/path/*",
			},
		},
	}

	securityDevOpsDiffs := []models.MergeRequestDiff{
		{NewPath: "/terraform/1", OldPath: "/not/tracked/1"},
		{NewPath: "/path/1", OldPath: "/path/1"},
	}

	mockDiffsJSONString, err := json.Marshal(securityDevOpsDiffs)
	assert.Nil(err)

	mockHTTPResponse2 := &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(bytes.NewReader(mockDiffsJSONString)),
	}

	clientErr := fmt.Errorf("client error")

	mockAPIClient.On("Do", mock.Anything).Return(mockHTTPResponse2, clientErr)

	ops, err := operations.NewLabelerOperations(apiclient)
	assert.Nil(err)

	_, err = ops.ReconcileDiffs(testCtx, providedConfig)
	assert.Error(err)
	assert.ErrorContains(err, "error in apiclient")
}

func TestSquashLabelsToString(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)

	labelList := []models.Label{{Name: "devops"}, {Name: "infra"}}

	labelListResults := operations.SquashLabelsToString(labelList)

	assert.Equal(labelListResults, "devops,infra")

	singleItemList := []models.Label{{Name: "one"}}

	singleListResult := operations.SquashLabelsToString(singleItemList)

	assert.Equal(singleListResult, "one")
}
