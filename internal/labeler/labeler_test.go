package labeler_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/gitlab"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/labeler"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/operations"
)

var groupID = ""

func TestNewGitlabMergeReqestLabeler(t *testing.T) {
	t.Parallel()
	testCtx := context.Background()

	assert := assert.New(t)

	configPath := "test"
	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,
		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	testOps, err := operations.NewLabelerOperations(apiclient)
	if err != nil {
		t.Fail()
	}

	_, err = labeler.NewGitlabMergeRequestLabeler(testCtx, testOps, configPath)
	assert.Nil(err)
}

func TestNewGitlabMergeReqestLabeler_BadArg(t *testing.T) {
	t.Parallel()
	testCtx := context.Background()

	assert := assert.New(t)

	badConfigPath := ""
	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,
		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	testOps, err := operations.NewLabelerOperations(apiclient)
	if err != nil {
		t.Fail()
	}

	_, secondErr := labeler.NewGitlabMergeRequestLabeler(testCtx, testOps, badConfigPath)
	assert.NotNil(secondErr)
}

func TestNewGitlabMergeReqestLabeler_BuildConfig(t *testing.T) {
	t.Parallel()
	testCtx := context.Background()

	assert := assert.New(t)

	configPath := "test"
	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	testOps, err := operations.NewLabelerOperations(apiclient)
	if err != nil {
		t.Fail()
	}

	lb, err := labeler.NewGitlabMergeRequestLabeler(testCtx, testOps, configPath)
	assert.Nil(err)

	label1 := &models.Label{Name: "label1"}
	label2 := &models.Label{Name: "label2"}

	testTargetList := []models.LabelConfig{
		{Label: *label1, Paths: []string{"/path/1"}},
		{Label: *label2, Paths: []string{"/path/2", "/path/tes/23", "/config/*"}},
	}

	yamlData := `
label1:
- /path/1
label2:
- "/path/2"
- "/path/tes/23"
- "/config/*"
`
	configData, err := lb.BuildConfig([]byte(yamlData))
	assert.Nil(err)
	assert.Equal(configData, testTargetList)
}

func TestNewGitlabMergeReqestLabeler_BuildConfig_badYaml(t *testing.T) {
	t.Parallel()
	testCtx := context.Background()

	assert := assert.New(t)

	configPath := "test"
	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	testOps, err := operations.NewLabelerOperations(apiclient)
	if err != nil {
		t.Fail()
	}

	lb, err := labeler.NewGitlabMergeRequestLabeler(testCtx, testOps, configPath)
	assert.Nil(err)

	yamlData := `
label1:
  testObj: true
`
	_, err = lb.BuildConfig([]byte(yamlData))
	assert.NotNil(err)
	assert.Error(err)
	assert.ErrorContains(err, "check your config")

	yamlData2 := `
label1:
  testObj: true
valid:
- "/test-d/d"
`
	_, err = lb.BuildConfig([]byte(yamlData2))
	assert.NotNil(err)
	assert.Error(err)
	assert.ErrorContains(err, "check your config")
}

func TestNewGitlabMergeReqestLabeler_ReadConfig(t *testing.T) {
	t.Parallel()
	testCtx := context.Background()

	assert := assert.New(t)

	configPath := "./stubs/base-labels.yaml"
	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	testOps, err := operations.NewLabelerOperations(apiclient)
	if err != nil {
		t.Fail()
	}

	lblr, err := labeler.NewGitlabMergeRequestLabeler(testCtx, testOps, configPath)
	assert.Nil(err)

	fileData, err := lblr.ReadConfigData()
	assert.Nil(err)
	assert.NotNil(fileData)

	testLabel := &models.Label{Name: "devops"}
	configTarget := models.LabelConfig{
		Label: *testLabel,
		Paths: []string{"config/*/values.yaml"},
	}

	configData, err := lblr.BuildConfig(fileData)
	assert.Nil(err)
	assert.NotNil(configData)
	assert.Equal(configData[0], configTarget)
}

func TestNewGitlabMergeReqestLabeler_ReadConfig_badFilePath(t *testing.T) {
	t.Parallel()
	testCtx := context.Background()

	assert := assert.New(t)

	configPath := "not-real/file.yml"
	token := "test"

	apiclient, err := gitlab.NewGitlabClient(
		token,

		"test",
		groupID,
		"test",
		gitlab.JobTokenMode,
		&http.Client{},
	)
	if err != nil {
		t.Fail()
	}

	testOps, err := operations.NewLabelerOperations(apiclient)
	if err != nil {
		t.Fail()
	}

	lblr, err := labeler.NewGitlabMergeRequestLabeler(testCtx, testOps, configPath)
	assert.Nil(err)

	fileData, err := lblr.ReadConfigData()
	assert.NotNil(err)
	assert.Error(err)
	assert.ErrorContains(err, "no such file or directory")
	assert.Nil(fileData)
}
