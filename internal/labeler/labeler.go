package labeler

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/models"
	"gitlab.com/rileyrohloff/gitlab-mr-labeler/internal/operations"
	"gopkg.in/yaml.v3"
)

var (
	InvalidConfigPathErrorString = "provided config-path value is either empty or nil"
	InvalidTokenErrorString      = "provided token value is either empty or nil"
	YamlConfigErrorString        = "error parsing yaml config, make sure to double check your config: %w"
	ConfigFileReadErrorString    = "error reading config file, error: %w, file: %v"
)

type GitlabMergeRequestLabeler struct {
	ctx        context.Context
	op         operations.LabelerOperations
	configPath string
}

func NewGitlabMergeRequestLabeler(
	ctx context.Context,
	ops operations.LabelerOperations,
	configPath string,
) (labeler GitlabMergeRequestLabeler, err error) {
	labeler.ctx = ctx

	if configPath == "" {
		return labeler, errors.New(InvalidConfigPathErrorString)
	}

	labeler.configPath = configPath
	labeler.op = ops

	return labeler, nil
}

func (l *GitlabMergeRequestLabeler) ReadConfigData() (data []byte, err error) {
	data, err = os.ReadFile(l.configPath)
	if err != nil {
		return data, fmt.Errorf(ConfigFileReadErrorString, err, l.configPath)
	}

	return data, nil
}

func (l *GitlabMergeRequestLabeler) BuildConfig(
	data []byte,
) ([]models.LabelConfig, error) {
	var yamlConfigData map[string][]string

	err := yaml.Unmarshal(data, &yamlConfigData)
	if err != nil {
		return nil, fmt.Errorf(
			YamlConfigErrorString,
			err,
		)
	}

	configData := make([]models.LabelConfig, 0)

	for k := range yamlConfigData {
		label := &models.Label{Name: k}
		labelCfg := &models.LabelConfig{
			Label: *label,
			Paths: yamlConfigData[k],
		}
		configData = append(configData, *labelCfg)
	}

	return configData, nil
}

func (l *GitlabMergeRequestLabeler) Run() error {
	log.Debug().Msgf("reading config data for: %v", l.configPath)

	fileData, err := l.ReadConfigData()
	if err != nil {
		return err
	}

	log.Debug().Msgf("attempting to build config")

	configData, err := l.BuildConfig(fileData)
	if err != nil {
		return err
	}

	log.Debug().Interface("configData", configData).Msg("config successfully built")

	fetchedLabels, err := l.op.GetUpstreamLabels(l.ctx)
	if err != nil {
		return err
	}

	log.Info().Interface("fetched data", fetchedLabels).Msg("found labels")

	log.Debug().Msg("validating config against upstream labels")

	isConfigValid := l.op.IsConfigValid(fetchedLabels, configData)

	if !isConfigValid {
		log.Error().
			Interface("providedConfig", configData).
			Interface("fetchedLabels", fetchedLabels).
			Msg("provided config failed validation")

		return fmt.Errorf(
			"failed validation, misaligned provided config with upstream's label config",
		)
	}

	log.Debug().Msg("provided config passed validation")

	if err := l.op.RunLabelerOperation(l.ctx, configData); err != nil {
		log.Error().Err(err).Msg("error in labeler operation")

		return fmt.Errorf("error in labeler operation: %w", err)
	}

	log.Debug().Msg("successfully ran labeler")

	return nil
}
